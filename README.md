# CopyAllCodeYouWantedToOneTxt
申请软件著作权，需要源码文档，此脚本用于遍历指定目录，读取指定文件，将其所有内容输出到某个指定文件里。
## 怎么跑？
- 你需要python3
- 编辑masct.py里的outputConfigs，配置目标文件与目录
```python
    outputConfigs = {
        "app_ios_part": {
            #注释格式
            "annotationFormat": "//",
            #这是目标文件后缀 数组
            "fileExtensions" : { "m", "h", "cpp", "c", "hpp" }, 
            #目标文件目录 数组
            "sourceDirNames" : { ".\\ios" },
            #输出至文件
            "outputFileName" : ".\\app_ios_part.txt", 
            #忽略
            "ignores": { } 
        }
    }
```
- python masct.py